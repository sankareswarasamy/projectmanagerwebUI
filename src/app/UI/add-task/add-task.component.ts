import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/MODELS/task';
import { User } from 'src/app/MODELS/user';
import { Parenttask } from 'src/app/MODELS/parenttask';
import { toDate } from '@angular/common/src/i18n/format_date';
import { ServiceService } from 'src/app/SERVICES/service.service';
import { Routes, RouterModule, Router, ActivatedRoute,NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  sortedOrder: string;
  sortedBy: string;
  IsParentTask:boolean;
  date = new Date();
  ptask:any;
  projectdisplay:any = 'none';
  userdisplay:any = 'none';
  parenttaskdisplay:any = 'none';
  projectsdata:  Array<any>;
  parentTaskData: Array<any>;
  usersdata:Array<any>;
  projectcolumns : Array<string> = ["Project_Name", "Priority", "Start_Date", "End_Date"];
  usercolumns : Array<string> = ["FirstName", "LastName", "EmployeeId"];
  parenttaskcolumns: Array<string> = ["Parent_ID", "Parent_Task"];
  searchProject:any;
  projectDetails:any = {};
  parenTaskDetails:any = {}
  taskDetails:any = {};
  taskID:any;
  task_ID_UPD:any; 
  edited:any = false;
  searchManager:any;
  searchParentTask:any;
parentTaskDetails:Parenttask =
{
Parent_ID:0,
Parent_Task:null

};

userDetails:User;
    
    
  constructor(private apiservice:ServiceService,private route: ActivatedRoute) { }

  ngOnInit() {

    console.log("Param: " + this.route.snapshot.params.taskId);
    this.task_ID_UPD = this.route.snapshot.params.taskId;
    this.taskDetails.Start_Date = new Date().toISOString().split('T')[0];
    this.taskDetails.End_Date = new Date();
    this.taskDetails.End_Date.setDate(this.taskDetails.End_Date.getDate() + 1);
    this.taskDetails.End_Date = this.taskDetails.End_Date.toISOString().split('T')[0];
    this.taskDetails.Priority = 0;
    if(this.route.snapshot.params.taskId != undefined)
    {
      this.edited = true;
      this.apiservice.getByTaskId(this.task_ID_UPD).subscribe(response=>
        {
  
          console.log("response",response);
          this.taskDetails = response;
          this.taskDetails.Start_Date = this.taskDetails.Start_Date.substr(0,10);
          this.taskDetails.End_Date = this.taskDetails.End_Date.substr(0,10);
          this.apiservice.getProjectData().subscribe(response=>
            {
       
              console.log("response",response);
              this.projectsdata = response;
              this.projectDetails = this.projectsdata.find(p=>p.Project_ID == this.taskDetails.Project_ID);  
              this.taskDetails.Project_Name = this.projectDetails.Project_Name;
            } );
            this.apiservice.getUserData().subscribe(response=>
              {
          
                console.log("response",response);
                this.usersdata = response;
                this.userDetails = this.usersdata.find(p=>p.User_ID == this.taskDetails.TASK_OWNER_ID);                
                this.taskDetails.UserName = this.userDetails.FirstName + " " + this.userDetails.LastName;
              } );
              if(this.taskDetails.Parent_ID != null)
              {
                this.apiservice.getParenTaskData().subscribe(response=>
                  {
              
                    console.log("response",response);
                    this.parentTaskData = response;
                    this.parenTaskDetails = this.parentTaskData.find(p=>p.Parent_ID == this.taskDetails.Parent_ID);  
                    this.taskDetails.ParentTask = this.parenTaskDetails.Parent_Task;
                  } );

              }
        } );
       
    }
  }
  addTask()
  {
    console.log(this.IsParentTask);
    if(this.IsParentTask == true)
    {
      this.parentTaskDetails.Parent_Task = this.taskDetails.TaskName;
      this.apiservice.postParentTask(this.parentTaskDetails)
      .subscribe(response=>
        {
  
          console.log("response",response);
          this.ptask = response;
          
        } );
        this.IsParentTask = false;
        this.taskDetails.TaskName = '';
        
    }
    else
    {
      console.log(this.taskDetails);
      this.taskDetails.Parent_ID = this.parenTaskDetails.Parent_ID;
      this.taskDetails.Project_ID = this.projectDetails.Project_ID;
      this.taskDetails.TASK_OWNER_ID = this.userDetails.User_ID;
      this.apiservice.postTaskData(this.taskDetails)
      .subscribe(response=>
        {
  
          console.log("response",response);
          
          //this.ptask = response;
        } );
        this.taskDetails = {};
    }

  }
  updateTask()
  {
    
      console.log(this.taskDetails);
      //this.taskDetails.Parent_ID = this.parenTaskDetails.Parent_ID;
      this.taskDetails.Project_ID = this.projectDetails.Project_ID;
      //this.taskDetails.TASK_OWNER_ID = this.userDetails.User_ID;
      this.apiservice.putTaskData(this.taskDetails)
      .subscribe(response=>
        {
  
          console.log("response",response);
          
          //this.ptask = response;
        } );
        this.taskDetails = {};
    
        this.edited = false;
  }
  projectModalDialog(){
    console.log('open')
    this.apiservice.getProjectData().subscribe(response=>
     {

       console.log("response",response);
       this.projectsdata = response;
     } );
   this.projectdisplay='block'; //Set block css
}
closeProjectModalDialog()
{
  this.projectdisplay='none'; //set none css after close dialog

}

getStartDate(startdate)
  {

    if(startdate != null)
    return startdate.substr(0,10);
    else    
    return startdate;
  }
  getEndDate(enddate)
  {    
    if(enddate != null)
    return enddate.substr(0,10);
    else    
    return enddate;
  }
selectparentask(parentask)
{

  this.parenTaskDetails = this.parentTaskData.find(p=>p.Parent_ID == parentask.Parent_ID);
  console.log('parentask' + this.parenTaskDetails.Parent_ID);
  this.taskDetails.ParentTask = this.parenTaskDetails.Parent_Task;
  this.taskDetails.Parent_ID = this.parenTaskDetails.Parent_ID;
  this.parenttaskdisplay = 'none';
}
selectuser(user)
{

  this.userDetails = {... this.usersdata.find(p=>p.User_ID == user.User_ID)};
  console.log('user' + this.userDetails.User_ID);
  this.taskDetails.UserName = this.userDetails.FirstName + " " + this.userDetails.LastName;
  this.taskDetails.TASK_OWNER_ID = this.userDetails.User_ID;
  this.userdisplay = 'none';
}
select(project)
{
  this.projectDetails = this.projectsdata.find(p=>p.Project_ID == project.Project_ID);
  console.log('project' + this.projectDetails.Project_ID);
  this.taskDetails.Project_Name = this.projectDetails.Project_Name;
  this.projectdisplay = 'none';
  this.taskDetails.Project_ID = this.projectDetails.Project_ID;
}
SearchParentTaskClick()
{
  this.apiservice.getParenTaskData().subscribe(response=>
    {

      console.log("response",response);
      this.parentTaskData = response;
    } );
    this.parenttaskdisplay='block'; //Set block css
}
userOpenModalDialog()
{
  this.apiservice.getUserData().subscribe(response=>
    {

      console.log("response",response);
      this.usersdata = response;
    } );
  this.userdisplay='block'; //Set block css

}
closeParenTaskModalDialog()
{

  this.parenttaskdisplay='none';
}
closeUserModalDialog()
{

  this.userdisplay = 'none';
}
reset()
{

  this.taskDetails = {};
  this.taskDetails.Start_Date = new Date().toISOString().split('T')[0];
    this.taskDetails.End_Date = new Date();
    this.taskDetails.End_Date.setDate(this.taskDetails.End_Date.getDate() + 1);
    this.taskDetails.End_Date = this.taskDetails.End_Date.toISOString().split('T')[0];
  this.edited = false;
}
ClearParentTask()
{
this.taskDetails.ParentTask = null;
this.taskDetails.Parent_ID = null;

}
IsParentTaskClick()
{

  this.taskDetails.Project_ID = null;
  this.taskDetails.Parent_ID = null;
  this.taskDetails.TASK_OWNER_ID = null;
  this.taskDetails.Project_Name = null;
  this.taskDetails.Parent_Task = null;
  this.taskDetails.UserName = null;
}
}
