import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProjectComponent } from './add-project.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import {Pipe, PipeTransform} from '@angular/core';
import { User } from 'src/app/MODELS/user';
import { Project } from 'src/app/MODELS/project';
@Pipe({
  name: 'projectfilterPipe'
})
export class ProjectFilterPipePipe implements PipeTransform {

  transform(projects: Project[], searchTerm:string): Project[] {
    if(!projects || (!searchTerm))
    {
      

      return projects;
    }
    else    
    {
      return projects.filter(projects=>
        projects.Project_ID.toString() === searchTerm
      || projects.Project_Name.toString().toLowerCase() === searchTerm.toLowerCase()
      || projects.Priority.toString() === searchTerm
      || (projects.Start_Date != null && projects.Start_Date.toString()) === searchTerm
      || (projects.End_Date != null && projects.End_Date.toString()) === searchTerm
      );
    }
  }

}
@Pipe({
  name: 'userfilterPipe'
})
export class FilterPipePipe implements PipeTransform {

  transform(users: User[], searchTerm:string): User[] {
    if(!users || (!searchTerm))
    {
      

      return users;
    }
    else    
    {
      return users.filter(users=>
        users.EmployeeId.toString() === searchTerm
      || users.FirstName.toString().toLowerCase() === searchTerm.toLowerCase()
      || users.LastName.toString().toLowerCase() === searchTerm.toLowerCase()
     
      );
    }
  }

}
describe('AddProjectComponent', () => {
  let component: AddProjectComponent;
  let fixture: ComponentFixture<AddProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientModule, RouterTestingModule,FormsModule],
      declarations: [ AddProjectComponent,FilterPipePipe,ProjectFilterPipePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.display = 'none';
  });
  // it('should test add', () => {
  //   component.addProject();
  //   expect(component.projectDetails).toEqual({});
  // });
 
  
  it('should reset values', () => {
    component.projectDetails = [ {
      
       Project_ID:4,
       Project_Name:'Test',
       Start_Date: new Date(),
       End_Date: new Date(),
       Priority :12
 
    }];
  
      component.reset();
      expect(component.projectDetails).toEqual({});
    });

    
 
});
